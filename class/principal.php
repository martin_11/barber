<?php


//Class mere

abstract class principal {
    
    protected $pk= "";
    protected $table="";
    protected $champs = [];
    protected $values = [];
    protected $links = [];
    protected $linksVal = [];
    protected $forms = [];





    //construct

    public function __construct($pk = null)
    {
        if(!is_null($pk)){
            $this->loadByPk($pk);
        }

    }


    //load by id
    public function loadByPk($pk)
    {
        $sql = "SELECT * FROM `{$this->table}` WHERE `{$this->pk}`=:pk";
        $param = [":pk"=>$pk];
        $req = BBBselect($sql, $param);
        $ligne = $req->fetch(PDO::FETCH_ASSOC);
        if(!empty($ligne)){
            $this->setFromTab($ligne);
            return true;
        }else{
            $this->values[$this->pk]=0;
            return false;
        }
        
    }


    public function setFromTab($tab)
    {
        foreach($this->champs as $nomChamp){
            if(isset($tab["$nomChamp"])){
                $this->set($nomChamp, $tab["$nomChamp"]);
            }
        }
        return true;
    }



   //setter
    public function set($nomChamp, $val){
        if(!in_array($nomChamp, $this->champs)){
            echo "Champ $nomChamp a ete pas retouvre";
            return false;
        }

        $this->values[$nomChamp]= $val;
        return true;
    } 


    //getter
    public function get($nomChamp){
        if(!in_array($nomChamp, $this->champs)){
            echo "Errore champ $nomChamp inexiste";
            return false;
        }

        if(isset($this->links[$nomChamp])){
            if(!isset($this->linksVal[$nomChamp])){
                $class = $this->links[$nomChamp];
                $this->linksVal[$nomChamp] = new $class($this->values[$nomChamp]);
            }
            return $this->values[$nomChamp];
        }

        if(isset($this->values[$nomChamp])){
            return $this->values[$nomChamp];
        }else{
            return "";
        }
    }




    //insert    

    public function insert(){
        $sql ="INSERT INTO `{$this->table}` SET ";
        $param =[];
        $set = [];

        foreach($this->champs as $nomChamp){
            if($nomChamp !== "id"){
                $set[]= "`$nomChamp`=:$nomChamp";
                $param[":$nomChamp"] = $this->values[$nomChamp];
            }
        }

        $sql .= implode(",", $set);
        if(BDDquery($sql, $param)===1){
            $this->values[$this->pk]=BDDlastId();
            return true;
        }else{
            echo "Errore d'insertion " . get_class($this);
            return false;
        }
    }



    //update 

    public function update(){
        $sql = "UPDATE `{$this->table}` SET ";
        $param = [];
        $set = [];

        foreach($this->champs as $nomChamp){
            $set[]= "`$nomChamp`=:$nomChamp";
            $param[":$nomChamp"] = $this->values[$nomChamp];
        }

        $sql .= implode(",", $set);
        $sql .= " WHERE `{$this->pk}` =:pk";
        $param[":pk"]= $this->values[$this->pk];
        if(BDDselect($sql, $param)!==-1){
            return true;
        }else{
            echo "Errore de modofication " . get_class($this);
            return false;
        }
    }


    //delete
    
    public function delete(){
        $sql ="DELETE * FROM `{$this->table}` WHERE `{$this->pk}`=:pk";
        $param = [":pk"=>$this->values[$this->pk]];
        if(BDDselect($sql , $param)!== -1){
            return true;
        }else{
            echo "Errore de suppression " . get_class($this);
            return false;
        }



    }


    //liste
    public function liste($order, $limit){
        $sql = "SELECT * FROM `{$this->table}` ORDER BY $order DESC LIMIT $limit";
        $param = [""];
        $req = BDDselect($sql, $param);
        $result = [];
        $class = get_class($this);
        while($ligne = $req->fetch(PDO::FETCH_ASSOC)){
            $class = new $class;
            $class->setFromTab($ligne);
            $result[$ligne["id"]] = $class;
        }

        return $result;
    } 








}